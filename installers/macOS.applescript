set Origin to "Macintosh HD/Users/user/Desktop/"
set desktopFdrPosix to quoted form of POSIX path of Origin
set destFolderName to "Test Folder 2"
set destinationFdrPosix to quoted form of desktopFdrPosix & POSIX file destFolderName
set Macarena to "macarena.py"
set sourceFnPosix to quoted form of desktopFdrPosix & POSIX file Macarena


set installation to (display dialog "Do you want to install Macarena Library?" 
    with title "Macarena"
    buttons {"Cancel", "Install"}
    cancel button 1
    default button 2)

set choice to button returned of installation

if choice is equal to "Install" then
    tell application "Finder"
        copy file sourceFnPosix to folder destinationFdrPosix
    end tell 
end if