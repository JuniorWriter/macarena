# -*- coding: utf-8 -*-

# Macarena - Basic functions to make console programs faster
#
# Macarena 1.0.0
#
# Copyright (c) 2020 by Andrés Galván

"""
Macarena - Basic functions to make console programs faster.

A simple module for basic side functions that allow the developer
to focus on the main process.

For a deep comprehension of the module, please, read the manual.
"""

import time
import platform
import getpass
import subprocess as sub
import re
import random
from colorama import Fore

# Clear console deppending of the O.S
def clear_console():
    ops = platform.system()
    
    if ops == 'Windows':
        sub.run('cls')
    else:
        sub.run('clear')
    


# List of errors
def error_msg(error_type):
    # Print an error message deppending of his type.
    error_list = list()
    error_list = ['Error List Working.', #0
    'Error 101. Only input numbers.', #1
    'Error 102. Only input positive numbers.', #2
    'Error 103. Option out of range.', #3
    'Error 104. Variable type not supported.', #4
    'Error 105. Only input negative numbers', #5
    'Error 106. Only input alphabetic letters', #6
    'Error 107. You should input something.'] #7

    if error_type == 0:
        print(Fore.GREEN + error_list[error_type])
    else:
        print(Fore.RED + error_list[error_type])
    print(Fore.WHITE)



# Syntax errors
def syntax_msg(error_type):   
    syntax_l = list()
    syntax_l = ['Syntax List Working.',
    'In function "casting_InLoop()".\n'
    '- Put an argument of "n" for allow negatives values.\n'
    '- Put an argument of "p" for allow only positives values.\n'
    '- Put an argument of "-n" for allow only negatives values.']

    if error_type == 0:
        print(Fore.GREEN + syntax_l[error_type])
    else:
        print('\n' + Fore.RED + '\tMacarena Syntax Error :/\n')
        print(Fore.WHITE + "--------------------------------------------")
        print(syntax_l[error_type])
    print(Fore.WHITE)



#Repeating menu
def repeat_menu():
    # Repeat a whole program using the repeat_menu function.
    err_count = 0
    repeat = True
    while (repeat):
        getpass.getpass('')
        if err_count >= 1:
            clear_console()
        
        print('\n\tSelect an option:\n')
        choice = input('[1] Repeat.            [2] EXIT. \n\n> ')
        try:
            choice = int(choice)
        except:
            repeat = True
            err_count += 1
            error_msg(1)
            continue
        
        if choice > 2 or choice < 1:
            error_msg(3)
            err_count += 1
        else: repeat = False

    return choice



#-------------------------------------------------------------
#CASTING
def casting_input(validation_arg, type_var):
    # Verify if an input can be casted in a specific type of variable
    try:
        if type_var == int:
            validation_arg = float(validation_arg)
        validation_arg = type_var(validation_arg)
    except:
        if type_var == int or type_var == float:
            error_msg(1)
        else:
            error_msg(4)
        quit()
    
    return validation_arg



# Cast an argument in a loop
def casting_input_InLoop(type_var, cartesian, value_arg):
    '''
    Verify if an input can be casted in a specific type of variable
    and keep asking until the user input a correct value.
    '''
    m_menu = False
    try:
        if type_var == int:
            value_arg = float(value_arg)
        value_arg = type_var(value_arg)
    except:
        if type_var == int or type_var == float:
            error_msg(1)
        else:
            error_msg(4)
        getpass.getpass('')
        m_menu = True
    
    if m_menu == False:
        if (cartesian == 'a' or cartesian == 'A'):
            pass
        elif (cartesian == 'p' or cartesian == 'P'):
            if value_arg < 0: 
                error_msg(2)
                getpass.getpass('')
                m_menu = True
        elif (cartesian == 'n' or cartesian == 'N'):
            if value_arg > 0:
                error_msg(5)
                getpass.getpass('')
                m_menu = True
        else:
            syntax_msg(1)

    return m_menu, value_arg



# Cast n lot of arguments in a loop
def casting_InLoop(type_var, cartesian, *args):
    '''
    Verify one or more arguments can be casted in a specific type of
    variable and keep asking until the user input a correct value.
    '''
    values = list()
    m_menu = False
    count = len(args)
    mac_error = False

    try:
        for each_value in args:
            if type_var == int:
                each_value = float(each_value)
            new_type = type_var(each_value)
            values.append(new_type)
    except:
        if type_var == int or type_var == float:
            error_msg(1)
        else:
            error_msg(4)
        getpass.getpass('')
        m_menu = True
        values = []
        for i in range (count):
            values.append(0)

    if m_menu == False:
        for each_value in values:
            if (cartesian == 'a' or cartesian == 'A'):
                break
            elif (cartesian == 'p' or cartesian == 'P'):
                if each_value < 0: 
                    error_msg(2)
                    getpass.getpass('')
                    m_menu = True
            elif (cartesian == 'n' or cartesian == 'N'):
                if each_value > 0:
                    error_msg(5)
                    getpass.getpass('')
                    m_menu = True
            else:
                mac_error = True
        if mac_error == True:
            syntax_msg(1)

    return m_menu, values



# Only allow alphabetic chars as input
def input_name(*args):
    '''
    Get one or more names as an argument and validate if is there
    numbers in the string, if so, ask for the input again until the
    input be a non-numeric. 
    '''
    name_error = False
    loop_menu = False
    
    for name in args:
        for lett in name:
            if re.fullmatch('[0-9]', lett):
                print('\n"{0}" isn\'t a valid name.'.format(name))
                error_msg(6)
                name_error = True
                break
            
        spacesless = name.split()
        if spacesless == []:
            error_msg(7)
            name_error = True
            break
            
        if len(name) < 3:
            print('\n"{0}" isn\'t a valid name.'.format(name))
            print(Fore.RED + 'Name too short.')
            print(Fore.WHITE)
            name_error = True
            break
               
    if (name_error):        
        loop_menu = True
        
    return loop_menu, args



# Output a Matrix
def show_matrix(M):
        '''Print the matrix's values in a table like'''
        rows = len(M)
        columns = len(M[0])
        for i in range(rows):
            for j in range(columns):
                print("\t{0} ".format(M[i][j]), sep=',', end='')
            print('\n')



# Matrix builder
def matrix_constructor(row, column):
    '''
    Make a matrix from user or developer's assignation for rows and columns,
    then ask for each value in every position to fill the matrix.
    '''
    matrix = []
    tmp = []
    
    if row < 1:
        print(Fore.RED + '\nThe matrix can not be created because rows must be greater than 0')
        print(Fore.WHITE)
    elif column < 1:
        print(Fore.RED + '\nThe matrix can not be created columns must be greater than 0')
        print(Fore.WHITE)
    else:
        count = 0
        for r in range (row):
            for c in range (column):
                value = int(input(f'Enter a value for [{r}][{c}]: '))                
                tmp.append(value)
                count += 1
                if count >= column:
                    matrix.append(tmp)
                    count = 0
                    tmp = []

        print('\n')
        show_matrix(matrix)



# Random Matrix gen
def matrix_gen(row, column):
    '''
    Make a matrix from user or developer's assignation for rows and columns,
    and fill it with random numbers
    '''
    
    matrix = []
    tmp = []
    
    if row < 1:
        print(Fore.RED + '\nThe matrix can not be created because rows should be greater than 0')
        print(Fore.WHITE)
    elif column < 1:
        print(Fore.RED + '\nThe matrix can not be created columns rows should be greater than 0')
        print(Fore.WHITE)
    else:
        count = 0
        for r in range (row):
            for c in range (column):
                value = random.randint(0, 101)            
                tmp.append(value)
                count += 1
                if count >= column:
                    matrix.append(tmp)
                    count = 0
                    tmp = []
            
        print('\n')
        show_matrix(matrix)
        
        
        
# Matrix 